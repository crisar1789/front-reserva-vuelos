import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Reserva } from '../reserva/reserva';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-consultareserva',
  templateUrl: './consultareserva.component.html',
  styleUrls: ['./consultareserva.component.css']
})
export class ConsultareservaComponent implements OnInit {

  private url = 'http://localhost:8080/back-reserva-vuelo/jaxrs/reserva';
  
  listaReservas : Reserva[] = [];

  constructor(private http: Http) { }

  ngOnInit() {
    
    this.getPromise().then(result => {

      if (result instanceof Array) {

        this.listaReservas = result;
      }
    },
      (err) => {
        alert(err);
      });
  }

  private getPromise()  {

    return new Promise(resolve => {
      this.http.get(this.url, { headers : this.getHeaders() }).map(response => response.json())
        .subscribe(
        response => {
          alert(response);
          if (response) {
            resolve(response);
          } else {
            resolve(false);
          }
        }, error => {
          alert('Error http');
          resolve(false);
        }
        );
    });
  }

  private getHeaders() {
    let headers = new Headers({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/x-www-form-urlencoded;'
    });
    
    return headers;
  }
}