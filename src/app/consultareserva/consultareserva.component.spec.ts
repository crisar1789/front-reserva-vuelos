import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultareservaComponent } from './consultareserva.component';

describe('ConsultareservaComponent', () => {
  let component: ConsultareservaComponent;
  let fixture: ComponentFixture<ConsultareservaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultareservaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultareservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
