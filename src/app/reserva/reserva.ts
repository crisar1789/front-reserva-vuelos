/**
 * New typescript file
 */
import { Timestamp } from 'rxjs';

export interface Reserva{
  id: number;
  idVuelo: number;
  nroIdentificacion: String;
  tipoIdentificacion: String;
  nombres: String;
  apellidos: String;
}