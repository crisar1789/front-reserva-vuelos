import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Reserva } from './reserva';
import { Vuelo } from '../vuelo/vuelo';
import { ReservaService } from './reserva.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {

  private url = 'http://localhost:8080/back-reserva-vuelo/jaxrs/reserva';

  reserva: Reserva;
  vuelo: Vuelo;
  response: String;

  idVuelo: number;
  nroVuelo: String = '';
  tpIdent: String = '';
  nroIdent: String = '';
  nombres: String = '';
  apellidos: String = '';
 
 
  constructor(private http: Http,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {

    }
 
  saveReserva() {
    this.reserva = {id: 0,
      idVuelo: this.idVuelo,
      nroIdentificacion: this.nroIdent,
      tipoIdentificacion: this.tpIdent,
      nombres: this.nombres,
      apellidos: this.apellidos};

      this.response = '';

      this.http.post(this.url, JSON.stringify(this.reserva), { headers : this.getHeaders() }).subscribe((r : Response) => {
        this.response = r.statusText;
      });

      this.nroIdent = '';
      this.tpIdent = '';
      this.nombres = '';
      this.apellidos = '';

      console.log(this.response);
  }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Content-Type', 'application/json');
    return headers;
  }
}
