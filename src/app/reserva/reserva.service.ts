import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Reserva } from './reserva';
import 'rxjs/add/operator/map';
 
@Injectable()
export class ReservaService {
  
  private url = 'http://localhost:8080/back-reserva-vuelo/jaxrs/reserva';
 
  constructor(private http: Http) {
  }

  get(id: number): Observable<Reserva[]> {
    let reserva$ = this.http
      .get(`${this.url}/${id}`, {headers: this.getHeaders()})
      .map(mapReservas);
      return reserva$;
  }
 
  getAll(): Observable<Reserva[]> {
    let reserva$ = this.http
      .get(this.url, {headers: this.getHeaders()})
      .map(mapReservas);
    return reserva$;
  }
 
  save(reserva: Reserva): Observable<Response> {
    return this.http.put(this.url, JSON.stringify(reserva), {headers: this.getHeaders()});
  }
 
  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return headers;
  }
}
 
function mapReservas(response: Response): Reserva[] {
  return response.json().map(toReserva);
}
 
function mapReserva(response: Response): Reserva {
  return toReserva(response.json());
}
 
function toReserva(r: any): Reserva {
  return r;
}