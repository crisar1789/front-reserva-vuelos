import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { VueloComponent } from './vuelo/vuelo.component';
import { ReservaComponent } from './reserva/reserva.component';
import { ConsultareservaComponent } from './consultareserva/consultareserva.component';

export const router: Routes = [
    { path: 'vuelo', component: VueloComponent },
    { path: 'reserva', component: ReservaComponent },
    { path: 'consultaReserva', component: ConsultareservaComponent }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);