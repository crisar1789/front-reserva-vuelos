import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routes } from './app.router'
import { AppComponent } from './app.component';
import { VueloComponent } from './vuelo/vuelo.component';
import { ReservaComponent } from './reserva/reserva.component';
import { ConsultareservaComponent } from './consultareserva/consultareserva.component';


@NgModule({
  declarations: [
    AppComponent,
    VueloComponent,
    ReservaComponent,
    ConsultareservaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
