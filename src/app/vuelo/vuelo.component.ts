import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Vuelo } from './vuelo';
import { Pais } from './pais';
import { Ciudad } from './ciudad';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-vuelo',
  templateUrl: './vuelo.component.html',
  styleUrls: ['./vuelo.component.css']
})
export class VueloComponent implements OnInit {

  private url = 'http://localhost:8080/back-reserva-vuelo/jaxrs/vuelo';
  
  listaVuelos : Vuelo[] = [];

  constructor(private http: Http) { }

  ngOnInit() {
    
    this.getPromise().then(result => {
      if (result instanceof Array) {
        this.listaVuelos = result;
      }
    },(err) => {});
  }

  private getPromise()  {

    return new Promise(resolve => {
      this.http.get(this.url, { headers : this.getHeaders() }).map(response => response.json())
        .subscribe(
        response => {
          if (response) {
            resolve(response);
          } else {
            resolve(false);
          }
        }, error => {
          resolve(false);
        }
        );
    });
  }

  private getHeaders() {
    let headers = new Headers({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/x-www-form-urlencoded;'
    });
    
    return headers;
  }
}