import { Ciudad } from './ciudad'

/**
 * New typescript file
 */

export interface Vuelo{
  id: number;
  nroVuelo: String;
  idOrigen: Ciudad;
  idDestino: Ciudad;
  fecha: String;
  horaSalida: String;
  horaLlegada: String;
  sillasDisponibles: number;
  valor: number;
}