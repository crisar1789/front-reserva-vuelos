import { Pais } from './pais'

/**
 * New typescript file
 */

export interface Ciudad{
    id: number;
    idPais: Pais;
    nombre: String;
  }