import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Vuelo } from './vuelo';
import 'rxjs/add/operator/map';

@Injectable()
export class VueloService {
  
  private baseUrl: string = 'http://localhost:9999/back-reserva-vuelo/jaxrs';
  
  constructor(private http: Http) { }
  
  get(id: number): Observable<Vuelo> {
    let vuelo$ = this.http
      .get(`${this.baseUrl}/vuelo/${id}`, {headers: this.getHeaders()})
      .map(mapVuelo);
      return vuelo$;
  }
  
  getAll(): Observable<Vuelo[]> {
     let vuelo$ = this.http
      .get(`${this.baseUrl}/vuelo`, {headers: this.getHeaders()})
      .map(mapVuelos);
    return vuelo$;
  }
 
  save(vuelo: Vuelo): Observable<Response> {
    console.log('Saving vuelo ' + JSON.stringify(vuelo));
    return this.http.put(`${this.baseUrl}/vuelo`, JSON.stringify(vuelo), {headers: this.getHeaders()});
  }
 
  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return headers;
  }
}
 
function mapVuelos(response: Response): Vuelo[] {
  return response.json().map(toVuelo);
}
 
function mapVuelo(response: Response): Vuelo {
  return toVuelo(response.json());
}
 
function toVuelo(r: any): Vuelo {
  return r;
}
